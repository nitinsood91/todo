const x=10;
console.log(x);

var storage;
var addNewItemList_tmp='';
var defaultItem='';
var clonedchild;
var length=0;
var fired=false;
var notes={
	getLists:function(){
		var x= document.querySelectorAll(".list,.list_new");
		for(var i=0;i<x.length;i++){
			if(x[i].getAttribute("class")=="list"){
				x[i].setAttribute("class","list_new");
				
			}
			else{
				x[i].setAttribute("class","list");
				
			}
		}
	},
	getNotes:function(){
		var x= document.querySelectorAll(".note,.note_new");
		for(var i=0;i<x.length;i++){
			if(x[i].getAttribute("class")=="note"){
				x[i].setAttribute("class","note_new");
			}
			else{
				x[i].setAttribute("class","note");
				
			}
	    }

	},
	addNewItem:function(){
			// var elemnt=document.getElementsByClassName("clone");
		
		var cln = clonedchild[length-1].cloneNode(true);

		document.querySelector("form").insertBefore(cln,document.querySelector(".submit"));		
	},
	addNotes:function(list=0){
		if(list==1){
			// var addNewItemList=addNewItemList_tmp;
			defaultItem='<div class="list-item">\
				<input type="checkbox"  name="completed">\
				<input type="text" class="listItemsText" name="listItems" value="Add New Item">\
				<input type="submit" name="delete" value="delete">\
			</div>';
		} 
		else{
			var addNewItemList='';
		}
		
			console.log("hi");
			document.querySelector(".content").innerHTML='<div>\
				<form onsubmit="return false">\
					<input class="title" type="text" name="title" value="Title"  >\
					<input class="description" type="text" name="description" value="Description" >\
					<div class="list-items">'+defaultItem+'</div>\
					<input class="submit" type="submit" name="submit" value="submit">\
				</form>\
			</div>';
		


	},
	inputFocus:function(i){
		if(i.value==i.defaultValue){ 
    		i.value=""; 
    	}
	},
	inputBlur:function(i){
		
		if(i.value==""){ 
    		i.value=i.defaultValue; 
    		
   		}
	},
	noteSubmit:function(valueEdit=0){
		var counter = localStorage.getItem("counter");
		

		if(document.querySelector(".title").value!=this.defaultValue && document.querySelector(".description").value!=this.defaultValue){
				
				var values={
							"title":document.querySelector(".title").value,
							"description":document.querySelector(".description").value
				};
				if(!counter){
					counter = 0;
				}
				if(valueEdit==0){
					counter++;
					localStorage.setItem("counter",counter);
				}
				else{
					counter=valueEdit;
				}
				
				localStorage.setItem(counter,JSON.stringify(values));
				
				
		}
		if(valueEdit==0){
			document.querySelector(".content").innerHTML="Note Added Successfully";
		}
		else{
			document.querySelector(".content").innerHTML="Note Edited Successfully";
		}
		
		
	},
	noteDisplay:function(){
		storage={};
		var values=localStorage.getItem("counter");
		for(var i=1;i<=values;i++){
			var key=localStorage.getItem(i);
			if(key){
				storage[i]=JSON.parse(localStorage.getItem(i.toString()));
			}
		}
		var content='';
		console.log(Object.keys(storage));
		var keys=Object.keys(storage);
		for(var i in keys){
			
			content+=
			'<div class="storage" id='+"note_"+keys[i]+' data-id='+keys[i]+'>\
				<span>'+storage[keys[i]].title+'</span>\
				<span>'+storage[keys[i]].description+'</span>\
				<a>Edit</a>\
				<a>Delete</a>\
			</div>';
		}
		document.querySelector(".content").innerHTML=content;
	},
	editNote:function(id){
		console.log(storage[id].title);
		document.querySelector(".content").innerHTML='<div>\
			<form onsubmit="return false">\
				<input class="title" id ="title_'+id+'" type="text" name="title" value="'+storage[id].title+'"  >\
				<input class="description" id="description_'+id+'" type="text" name="description" value="'+storage[id].description+'" >\
				<input class="edit" type="submit" name="edit" value="edit">\
				<input class="delete" type="submit" name="delete" value="delete">\
			</form>\
		</div>';
	},
	noteDelete:function(id){
		localStorage.removeItem(id.toString());
		document.querySelector(".content").innerHTML="Note Deleted successfully";
	},
	addNewLine: function() {
		if (this.querySelector("input").value != "") {
			var clone = this.cloneNode(true);
			this.parentNode.appendChild(clone);
		}
   		   
		notes.bindNewLine();
	},
	bindNewLine: function() {
		var list_items=document.querySelectorAll(".list-item");
		for(var i=0;i<list_items.length;i++){
			document.querySelectorAll(".list-item")[i].removeEventListener("keydown", this.addNewLine);
			document.querySelectorAll(".list-item")[i].removeEventListener("change", this.addNewLine);
		}
		
		// var list_items_length=document.querySelectorAll(".list-item").length;
		// var list_items_length_storage=0;
		// if(list_items_length==1){
		// 	list_items_length_storage=list_items_length;
			
		// }
		// else if((list_items_length!=1) &&(list_items_length_storage!=list_items_length)){
		// 	fired=false;
		// 	list_items_length_storage=list_items_length;
			
		// }
		document.querySelector(".list-item:last-child").addEventListener("keydown",this.addNewLine);
		document.querySelector(".list-item:last-child").addEventListener("change",this.addNewLine);

		
	}
};

var lists={
	addLists:function(){

	}
};

document.body.onload = function(){
	
	document.querySelector(".display_lists").onclick= function(){
		notes.getLists();
	}
	
	document.querySelector(".display_notes").onclick= function(){
		notes.getNotes();
	}

	document.querySelector("#addNote").onclick= function(){
		notes.addNotes();

		document.querySelector(".title").onfocus=function(){
			notes.inputFocus(this);
		}

		document.querySelector(".title").onblur=function(){
			notes.inputBlur(this);
		}
		document.querySelector(".description").onfocus=function(){
			notes.inputFocus(this);
		}

		document.querySelector(".description").onblur=function(){
			notes.inputBlur(this);
		}
		document.querySelector(".submit").onclick= function(){
			notes.noteSubmit();
		}
	}

	document.querySelector("#my_notes").onclick = function(){
		notes.noteDisplay();
		var links=document.querySelectorAll(".storage a");
		for(var i in links){
			links[i].onclick = function(){
				console.log(this.parentNode);
			notes.editNote(this.parentNode.getAttribute("data-id"));
				var id=this.parentNode.getAttribute("data-id");
				document.querySelector(".edit").onclick= function(){
					notes.noteSubmit(id);
				}
				document.querySelector(".delete").onclick= function(){
					notes.noteDelete(id);
				}

			}
		}
		
	}
	document.querySelector("#addList").onclick=function(){
		notes.addNotes(1);
		document.querySelector(".title").onfocus=function(){
			notes.inputFocus(this);
		}

		document.querySelector(".title").onblur=function(){
			notes.inputBlur(this);
		}
		document.querySelector(".description").onfocus=function(){
			notes.inputFocus(this);
		}

		document.querySelector(".description").onblur=function(){
			notes.inputBlur(this);
		}
		console.log("hi");
		document.querySelector(".list-item .listItemsText").onfocus=function(){
				notes.inputFocus(this);
		}

		document.querySelector(".list-item .listItemsText").onblur=function(){
			notes.inputBlur(this);
		}

		notes.bindNewLine();

		// document.querySelectorAll(".list-item")[(document.querySelectorAll(".list-item").length)-1].onkeydown = notes.addNewLine(); function() {
		// 	console.log(document.querySelectorAll(".list-item")[1]);
			
			
			
		// }

			// length=document.getElementsByClassName("clone").length;
			// clonedchild=document.getElementsByClassName("clone");
			// clonedchild[length-1].childNodes[2].onkeydown=function(){
			// 	notes.addNewItem();
			// }

	}
	
	
};



