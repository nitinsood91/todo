<html>
	<link rel="stylesheet" type="text/css" href="/todo/public/css/newNote.css">
	<body>
		<div class="wrapper">
			<div class="row">
				<div class="col1">
					<div class="menu">
						<ul>
							<li class="logo">TODO</li>
							<li><a href="" >Home</a></li>
							<li class="display_lists">Lists</li>
							<li class="list" id="my_lists">My Lists</li>
							<li class="list" id="addList">Add List</li>
							<li class="display_notes">Notes</li>
							<li class="note" id="my_notes">My Notes</li>
							<li class="note"  id="addNote">Add Note</li>
							<li><a href="/todo/progress-bar" >About Todo</a></li>
							<!-- <li class=""></li> -->
						</ul>
					</div>
				</div>
				<div class="col4">
					<div class="content"></div>
			    </div>
			</div>
		</div>
		<script type="text/javascript" src="/todo/public/js/newNote.js"></script>
	</body>
</html>